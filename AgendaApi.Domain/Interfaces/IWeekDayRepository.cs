﻿using AgendaApi.Domain.Entities;
using System.Linq.Expressions;

namespace AgendaApi.Domain.Interfaces
{
    public interface IWeekDayRepository : IBaseRepository<WeekDay>;
}
