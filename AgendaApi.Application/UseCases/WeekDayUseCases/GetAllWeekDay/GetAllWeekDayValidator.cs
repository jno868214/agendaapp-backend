﻿using FluentValidation;

namespace AgendaApi.Application.UseCases.WeekDayUseCases.GetAllWeekDay
{
    public class GetAllWeekDayValidator 
        : AbstractValidator<GetAllWeekDayRequest>;
}
