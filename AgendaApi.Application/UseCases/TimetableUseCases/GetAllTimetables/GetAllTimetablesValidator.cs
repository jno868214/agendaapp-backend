﻿using FluentValidation;

namespace AgendaApi.Application.UseCases.TimetableUseCases.GetAllTimetables
{
    public class GetAllTimetablesValidator 
        : AbstractValidator<GetAllTimetablesRequest>;
}
