﻿using AgendaApi.Application.UseCases.SchedulingUseCases.DTOs;

namespace AgendaApi.Application.UseCases.SchedulingUseCases.ConfirmeScheduling
{
    public sealed record EndsSchedulingResponse : SchedulingBaseResponse;
}
