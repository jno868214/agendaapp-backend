﻿using AgendaApi.Application.UseCases.ServiceUseCase.DTOs;

namespace AgendaApi.Application.UseCases.ServiceUseCase.GetAllService
{
    public sealed record GetAllServiceResponse : ServiceBaseResponse;
}
