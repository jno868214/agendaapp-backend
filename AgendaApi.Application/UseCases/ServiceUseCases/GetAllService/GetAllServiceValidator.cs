﻿using FluentValidation;

namespace AgendaApi.Application.UseCases.ServiceUseCase.GetAllService
{
    public class GetAllServiceValidator 
        : AbstractValidator<GetAllServiceRequest>;
}
