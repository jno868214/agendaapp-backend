﻿using AgendaApi.Application.UseCases.ServiceUseCase.DTOs;

namespace AgendaApi.Application.UseCases.ServiceUseCase.DeleteService
{
    public sealed record DeleteServiceResponse : ServiceBaseResponse;
}
